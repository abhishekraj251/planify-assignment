"""stocklist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from authentication.views import login_user, register, logout_user
from webapp.views import home_page, get_stocks, get_stock_detail

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_page, name='home'),
    path('login/', login_user, name='login'),
    path('register/', register, name='registration'),
    path('logout/', logout_user, name='logout'),
    path('stocks/', get_stocks, name='stocks'),
    path('stocks/<slug:slug>/', get_stock_detail, name='stock_detail')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

