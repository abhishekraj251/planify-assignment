from django.contrib import admin

from webapp.models import StockExchange, Stocks


class StockExchangeAdmin(admin.ModelAdmin):
    model = StockExchange
    list_display = ('name',)


class StocksAdmin(admin.ModelAdmin):
    model = Stocks
    prepopulated_fields = {'slug': ('symbol',)}
    list_display = ('symbol', 'listed_on_exchange', 'current_price', 'name')

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            'listed_on_exchange'
        )


admin.site.register(StockExchange, StockExchangeAdmin)
admin.site.register(Stocks, StocksAdmin)
