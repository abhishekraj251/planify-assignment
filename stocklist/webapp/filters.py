import django_filters
from django_filters import CharFilter

from .models import Stocks


class StockFilter(django_filters.FilterSet):
    symbol = CharFilter(lookup_expr='icontains', label='Symbol')

    class Meta:
        model = Stocks
        fields = [
            'symbol',
        ]
