from django.db import models
from core.models import TimeStampIndexed, NameAndDescription, Active


class StockExchange(NameAndDescription):

    def __str__(self):
        return '%s' % self.name


class Stocks(TimeStampIndexed, Active, NameAndDescription):
    symbol = models.CharField(max_length=20, db_index=True)
    listed_on_exchange = models.ForeignKey(StockExchange, related_name='listed_stocks', on_delete=models.CASCADE)
    current_price = models.DecimalField(max_digits=7, decimal_places=2)
    fifty_two_week_high = models.DecimalField(max_digits=7, decimal_places=2)
    fifty_two_week_low = models.DecimalField(max_digits=7, decimal_places=2)
    market_cap = models.DecimalField(max_digits=20, decimal_places=2)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return '%s' % self.symbol
