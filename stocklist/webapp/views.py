from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from webapp.filters import StockFilter
from webapp.models import Stocks, StockExchange


def home_page(request):
    context = {
        'data': [],

    }
    return render(request, "home.html", context)


@login_required(login_url='login')
def get_stocks(request):
    data = Stocks.objects.filter(is_active=True)
    symbol_filter = StockFilter(request.GET, data)
    context = {
        "symbol_filter": symbol_filter,
        "data": symbol_filter.qs
    }
    return render(request, "listed_stocks.html", context)


@login_required(login_url='login')
def get_stock_detail(request, slug):
    data = Stocks.objects.get(slug=slug)
    context = {
        "data": data
    }
    return render(request, "stock_detail.html", context)
