from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, forms
from django.contrib import messages

from authentication.forms import RegistrationForm
from authentication.url_helpers import redirect_to_home_page, open_login_page, redirect_to_login_page
from django.http import HttpResponse


def login_user(request):
    form = forms.AuthenticationForm(request, data=request.POST)
    if request.user.is_authenticated:
        return redirect_to_home_page()
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect_to_home_page()
                return HttpResponse('Account is not active at the moment.')
            else:
                messages.info(request, 'Username or Password is Incorrect')
        return open_login_page(request, context={"login_form": form})


def register(request):
    if request.user.is_authenticated:
        return redirect_to_home_page()
    else:
        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'Account successfully registered.')
                return redirect_to_login_page()
        else:
            form = RegistrationForm()

        context = {'form': form}
        return render(request, 'register.html', context)


def logout_user(request):
    logout(request)
    return redirect_to_home_page()
