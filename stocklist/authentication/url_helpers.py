from django.shortcuts import redirect, render


def redirect_to_login_page():
    return redirect('login')


def redirect_to_home_page():
    return redirect('home')


def open_login_page(request, context):
    return render(request, 'login.html', context)
