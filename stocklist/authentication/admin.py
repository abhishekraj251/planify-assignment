from django.contrib import admin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.urls import re_path as url
from django.contrib import messages
from authentication.forms import StaffChangeForm, StaffCreationForm
from authentication.models import *
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponseRedirect
from django.utils.html import escape
from django.contrib.auth.admin import sensitive_post_parameters_m
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.template.response import TemplateResponse


class StaffAdmin(admin.ModelAdmin):
    model = Staff
    change_user_password_template = None

    form = StaffChangeForm
    add_form = StaffCreationForm
    change_password_form = AdminPasswordChangeForm

    list_display = ('first_name', 'last_name', 'email', 'is_active', 'phone')
    list_filter = ('is_active', 'phone',)

    def queryset(self, request):
        qs = super(StaffAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(is_superuser=False)

    def get_urls(self):
        return [
                   url(r'^(\d+)/change/password/$', self.admin_site.admin_view(self.user_change_password))
               ] + super(StaffAdmin, self).get_urls()

    def change_password(self, request, id, user, form_url=''):
        if request.method == 'POST':
            form = self.change_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                msg = gettext('Password changed successfully.')
                messages.success(request, msg)
                return HttpResponseRedirect('..')
        else:
            form = self.change_password_form(user)

        fieldsets = [(None, {'fields': list(form.base_fields)})]
        admin_form = admin.helpers.AdminForm(form, fieldsets, {})

        context = {
            'title': _('Change password: %s') % escape(user.get_username()),
            'adminForm': admin_form,
            'form_url': form_url,
            'form': form,
            'is_popup': '_popup' in request,
            'add': True,
            'change': False,
            'has_delete_permission': False,
            'has_change_permission': True,
            'has_absolute_url': False,
            'opts': self.model._meta,
            'original': user,
            'save_as': False,
            'show_save': True,
        }
        return TemplateResponse(
            request, self.change_user_password_template or 'admin/auth/user/change_password.html', context
        )

    @sensitive_post_parameters_m
    def user_change_password(self, request, id, form_url=''):
        if not self.has_change_permission(request):
            raise PermissionDenied
        user = get_object_or_404(self.queryset(request), pk=id)
        if request.user.is_superuser:
            return self.change_password(request, id, user, form_url)
        elif request.user.is_staff:
            if user.is_superuser:
                raise PermissionDenied
            if not user.is_staff:
                return self.change_password(request, id, user, form_url)
            else:
                if user == request.user:
                    return self.change_password(request, id, user, form_url)
                else:
                    raise PermissionDenied
        else:
            raise PermissionDenied


admin.site.register(Staff, StaffAdmin)

