from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from core.models import TimeStampIndexed


class StaffUserManager(BaseUserManager):

    def create_user(self, phone, password=None, **extra_fields):
        """
        Creates and saves a User with the given phone and password.
        """
        now = timezone.now()
        if not phone:
            raise ValueError('The given phone must be set')
        user = self.model(
            phone=phone, is_staff=False, is_active=True, is_superuser=False, created_at=now, **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone, password, **extra_fields):
        u = self.create_user(phone, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)
        return u


class Staff(AbstractBaseUser, PermissionsMixin, TimeStampIndexed):
    id = models.AutoField(primary_key=True)
    email = models.EmailField(verbose_name='email address', max_length=255, db_index=True, null=True, blank=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    phone = models.BigIntegerField(unique=True, db_index=True)
    objects = StaffUserManager()
    USERNAME_FIELD = 'phone'

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Returns the short name for the user."""
        return self.first_name

    def __str__(self):
        name = self.name
        if self.phone:
            name += ", " + str(self.phone)
        return name.strip()

    def __unicode__(self):
        return self.__str__()

    @property
    def name(self):
        return '%s %s' % (self.first_name, self.last_name)

    @property
    def username(self):
        return '%s %s' % (self.first_name, self.last_name)

    def has_perm(self, perm, obj=None):
        # Deny for inactive or anonymous user
        if not self.is_active or obj is not None:
            return False
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True
        temp_perm = perm.split('.')[1]
        if hasattr(self, 'permissions'):
            if len(self.permissions) > 0:
                return temp_perm in self.permissions
        else:
            return super(Staff, self).has_perm(perm, obj)
        return False
