# README #


### Description ###

* Planify assessment

### How do I get set up? ###

* setup an optional virtual environment : python3 -m venv .env
* pip install -r requirements.txt


### A few pointers about this assessment: """

* For the sake of simplicity I have considered a stock to be listed on any one index.
* A different approach I could have followed is to have a separate model to map the stock exchange and the stock.
* That way it would have been possible to capture extra information such as:
     -Presence of stock and its price on multiple exchanges.
     -Listing date of that stock on a particular exchange.
     -Whether the trading is enabled for that stock on that particular exchange. etc.

* I didn't put much effort on the UI owing to the lack of time. Main goal has been to demonstrate the functionality.

* An alert popup reminds the non-logged-in user to log in every 3 minutes as long as they are on the home page.

* The search filter on the stock listing page is based on the symbol field.

* Webapp is the main app that handles stock data.

* I've included a db instance as well as a sample video in the /static/media directory.
